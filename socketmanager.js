var controller_creditrequest_instance	= require('./controllers/creditrequest.js');
var controller_dashboard_instance		= require('./controllers/dashboard.js');
var model_dashboard 					= require('./models/dashboard.js');

var controller_dashboard = controller_dashboard_instance(model_dashboard);
var controller_creditrequest = controller_creditrequest_instance(model_dashboard);

var SocketManager = function() {
	this.users_online = {};
};

SocketManager.prototype.listen = function(io) {

	this.creditrequest_interval = setInterval(function() {
		controller_creditrequest.update();
	}, 1000);

	io.sockets.on('connection', function(socket) {
		console.log('new connection');

		socket.on('login_user', function(data) {
			controller_dashboard.login_user(data, function(res) {
				socket.emit('response_login_user', res);
			});
		});

		socket.on('identify', function(data) {
			this.current_user = data.user;
			controller_dashboard.login_user(data, function(res) {});
			controller_creditrequest.add_client({user: data.user, socket: socket});
		});

		socket.on('new_creditrequest', function(data) {
			data['user'] = this.current_user;
			controller_dashboard.save_creditrequest(data);
		});

		socket.on('new_vote', function(data) {
			data['user'] = this.current_user;
			controller_dashboard.vote_creditrequest(data);
		});
			
		socket.on('disconnect', function() {
			controller_creditrequest.remove_client({user: this.current_user });
			controller_dashboard.logout_user({user: this.current_user });
		});
	});
};

module.exports = new SocketManager();
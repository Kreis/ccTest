var get_connection = require('./../database/connection.js');

var DashboardModel = function() {

};

/*
    callback token session for valid user
    callback empty value if user is already logged in
*/
DashboardModel.prototype.login_user = function(data, callback) {
    let token = '';

    let connection = get_connection();
    connection.connect(function(err) {
        if (err) throw err;

        connection.query('USE cctest_db', function(err, result) {
            if (err) throw err;
        });

        let valid_user = true;

        token = '_' + Math.random().toString(20).substr(2, 20);
        connection.query(
        `CALL proc_validate_user('${data.user}', '${token}', @result);
        SELECT @result;`,
        function(err, result) {
            if (err) throw err;

            valid_user = result[2][0]['@result'] == 1;

        });

        connection.end(function(err) {
            if (err) throw err;
            console.log('complete');

            if (valid_user)
                callback(token);
            else
                callback(0);
        });
    });
};

DashboardModel.prototype.delete_token = function(data) {
    let connection = get_connection();
    connection.connect(function(err) {
        if (err) throw err;

        connection.query('USE cctest_db', function(err, result) {
            if (err) throw err;
        });

        connection.query(`
            UPDATE user SET current_token = '' WHERE name = '${data.user}' ;`,
            function(err, result) {
                if (err) throw err;
            });

        connection.end(function(err) {
            if (err) throw err;
            console.log('complete');
        });
    });
};

DashboardModel.prototype.get_creditrequests_actives = function(callback) {
    let connection = get_connection();
    connection.connect(function(err) {
        if (err) throw err;

        connection.query('USE cctest_db', function(err, result) {
            if (err) throw err;
        });

        let response = 0;

        connection.query(`
            SELECT user FROM history WHERE result IS NULL ;`,
            function(err, result) {
                if (err) throw err;

                response = result;
            });

        connection.end(function(err) {
            if (err) throw err;

            callback(response);
        });
    });
};

DashboardModel.prototype.get_creditrequests_info = function(data, callback) {
    let connection = get_connection();
    connection.connect(function(err) {
        if (err) throw err;

        connection.query('USE cctest_db', function(err, result) {
            if (err) throw err;
        });

        let response = 0;

        connection.query(`
            SELECT h.start_date, h.result, SUM(vote) as vote_positive, COUNT(vote) AS vote_total, amount
            FROM history h
            LEFT JOIN votes v ON h.id = v.id_history
            WHERE h.user = '${data.user}'
            GROUP BY h.id ;`,
            function(err, result) {
                if (err) throw err;

                if ( ! result[ 0 ])
                    return;

                response = result;
            });

        connection.end(function(err) {
            if (err) throw err;

            callback(response);
        });
    });
};

DashboardModel.prototype.get_user_voted_creditrequest = function(data, callback) {
    let connection = get_connection();
    connection.connect(function(err) {
        if (err) throw err;

        connection.query('USE cctest_db', function(err, result) {
            if (err) throw err;
        });

        let response = 0;

        connection.query(`
            SELECT h.user
            FROM history h
            INNER JOIN votes v ON h.id = v.id_history
            WHERE v.user = '${data.user}' AND h.result IS NULL ;`,
            function(err, result) {
                if (err) throw err;

                if ( ! result[ 0 ])
                    return;

                response = result;
            });

        connection.end(function(err) {
            if (err) throw err;

            callback(response);
        });
    });
};

DashboardModel.prototype.close_creditrequest = function(data) {
    let connection = get_connection();
    connection.connect(function(err) {
        if (err) throw err;

        connection.query('USE cctest_db', function(err, result) {
            if (err) throw err;
        });

        connection.query(`
            UPDATE history SET result = '${data.result}'
            WHERE user = '${data.user}' AND result IS NULL ;`,
            function(err, result) {
                if (err) throw err;
            });

        connection.end(function(err) {
            if (err) throw err;
        });
    });
};

DashboardModel.prototype.save_creditrequest = function(data) {
    let connection = get_connection();
    connection.connect(function(err) {
        if (err) throw err;

        connection.query('USE cctest_db', function(err, result) {
            if (err) throw err;
        });

        connection.query(`
            INSERT INTO history (user, start_date, amount, credit_type)
            VALUES ('${data.user}', '${data.time}', '${data.amount}', '${data.credit_type}') ;`,
            function(err, result) {
                if (err) throw err;
            });

        connection.end(function(err) {
            if (err) throw err;
        });
    });
};

DashboardModel.prototype.vote_creditrequest = function(data) {

    let connection = get_connection();
    connection.connect(function(err) {
        if (err) throw err;

        connection.query('USE cctest_db', function(err, result) {
            if (err) throw err;
        });

        connection.query(`
            INSERT INTO votes (user, vote, id_history)
            ( SELECT '${data.user}', '${data.vote}', id FROM history WHERE user = '${data.user_request}' AND result IS NULL) ;`,
            function(err, result) {
                if (err) throw err;
            });

        connection.end(function(err) {
            if (err) throw err;
        });
    });
};


module.exports = new DashboardModel();
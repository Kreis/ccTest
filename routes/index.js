'use strict';

var express = require('express');
var path = require('path');
var router = express.Router();

router.get('/', function(req, res) {
	res.sendFile(path.resolve(path.join(__dirname, '../views', 'login.html')));
});

router.get('/dashboard', function(req, res) {
	res.sendFile(path.resolve(path.join(__dirname, '../views', 'dashboard.html')));
});

module.exports = router;

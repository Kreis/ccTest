'use strict';

const express = require('express');
const path = require('path');
var index = require('./routes/index');
var http = require('http');
var socketmanager = require('./socketmanager.js');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
var app = express();
var server = http.Server(app);

var io = require('socket.io')(server);
socketmanager.listen(io);

// console.log(path.join(__dirname, 'public'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/', index);

server.listen(PORT, HOST);
console.log('Running on http://' + HOST + ':' + PORT);


var CreditRequest = function(model_dashboard, context) {

	// 5 minutes
	this.time_for_request = 300000;
	// 10 seconds
	// this.time_for_request = 10000;
	// 1 minute
	// this.time_for_request = 60000;

	this.model_dashboard = model_dashboard;
	if (typeof(context) == 'undefined')
		this.context = this;
	else
		this.context = context;

	this.user_online = {};
	this.current_request = { available: true, nvalue: {} };
};

CreditRequest.prototype.add_client = function(data) {
	this.context.user_online[ data.user ] = {
		socket: data.socket,
		creditrequest_voted: {},
		history: []
	};
};

CreditRequest.prototype.remove_client = function(data) {
	delete this.context.user_online[ data.user ];
};

CreditRequest.prototype.send_data = function() {

	var callback_send = function(data) {
		this.context.user_online[ data.user ]['socket'].emit('update', data.package);
	}.bind(this);


	for (let id_user_online in this.user_online) {
		let package = [];
		let user_included_in_current_request = false;

		this.context.update_creditrequest_voted({ user: id_user_online });

		for (let id_user_request in this.current_request) {
			if (id_user_request == 'available' || id_user_request == 'nvalue')
				continue;

			if (this.user_online[ id_user_online ] && this.user_online[ id_user_online ]['creditrequest_voted'][ id_user_request ])
				continue;

			if (id_user_request == id_user_online)
				user_included_in_current_request = true;

			let pack = {
				user: 		id_user_request,
				positives: 	this.current_request[ id_user_request ]['positives'],
				negatives: 	this.current_request[ id_user_request ]['negatives'],
				count_down: this.current_request[ id_user_request ]['count_down'],
				amount: 	this.current_request[ id_user_request ]['amount'],
				history: 	this.current_request[ id_user_request ]['history']
			};

			package.push(pack);
		}

		if (user_included_in_current_request)
			callback_send({ user: id_user_online, package: package });
		else
			this.context.get_own_history({ user: id_user_online, package: package }, callback_send);
	}

};

CreditRequest.prototype.get_own_history = function(data, callback) {
	
	this.model_dashboard.get_creditrequests_info({ user: data.user }, function(data_info) {

		let my_history_data = {
			user: data.user,
			history: []
		};

		for (let i in data_info) {

			let without_votes = data_info[ i ]['vote_positive'] == null;
			let h_vote_positive = without_votes ? 0 : +data_info[ i ]['vote_positive'];
			let h_vote_negative = without_votes ? 0 : +data_info[ i ]['vote_total'] - +data_info[ i ]['vote_positive'];

			my_history_data.history.push({
				positives: h_vote_positive,
				negatives: h_vote_negative,
				result: data_info[ i ]['result']
			});
		}

		data.package.push(my_history_data);

		callback({ user: data.user, package: data.package });
	});
}

CreditRequest.prototype.update_creditrequest_voted = function(data) {
	var user_online = this.user_online;

	this.model_dashboard.get_user_voted_creditrequest(data, function(res) {

		let creditrequest_voted = {};
		for (let i in res) {
			let user = res[ i ]['user'];
			creditrequest_voted[ user ] = true;
		}

		user_online[ data.user ]['creditrequest_voted'] = creditrequest_voted;
	});
};

CreditRequest.prototype.update = function() {

	if ( ! this.current_request.available)
		return;

	this.context.send_data();

	var current_request 	= this.current_request;
	var model_dashboard		= this.model_dashboard;
	var time_for_request 	= this.time_for_request;
	var user_online 		= this.user_online;

	new Promise(function(success, reject) {
		model_dashboard.get_creditrequests_actives(success);
	}).then(function(data) {
		// for active creditrequest
		for (let i in data) {
			let user = data[ i ]['user'];

			let creditrequest_info = model_dashboard.get_creditrequests_info({user: user}, function(data_info) {

				if (data_info == 0)
					return;

				current_request.nvalue[ user ] = {
					history: [],
					count_down: 0,
					positives: 0,
					negatives: 0,
					amount: 0
				};

				// for info from creditrequest
				for (let i in data_info) {

					let currently_active_creditrequest = data_info[ i ]['result'] == null;
					if (currently_active_creditrequest) {
						current_request.nvalue[ user ]['count_down'] = +data_info[ i ]['start_date'] + time_for_request - +(new Date().getTime());
						current_request.nvalue[ user ]['amount'] 	= data_info[ i ]['amount']

						if (data_info[ i ]['vote_positive'] != null) {
							current_request.nvalue[ user ]['positives'] = +data_info[ i ]['vote_positive'];
							current_request.nvalue[ user ]['negatives'] = +data_info[ i ]['vote_total'] - +data_info[ i ]['vote_positive'];
						}

						if (+current_request.nvalue[ user ]['count_down'] <= 0) {

							let result_request = +current_request.nvalue[ user ]['positives'] > +current_request.nvalue[ user ]['negatives'];
							result_request = result_request ? 1 : 0;
							model_dashboard.close_creditrequest({user: user, result: result_request});
							delete current_request.nvalue[ user ];
						}

						continue;
					}

					let without_votes = data_info[ i ]['vote_positive'] == null;
					let h_vote_positive = without_votes ? 0 : +data_info[ i ]['vote_positive'];
					let h_vote_negative = without_votes ? 0 : +data_info[ i ]['vote_total'] - +data_info[ i ]['vote_positive'];

					current_request.nvalue[ user ]['history'].push({
						positives: h_vote_positive,
						negatives: h_vote_negative,
						result: data_info[ i ]['result'],
						amount: data_info[ i ]['amount']
					});
				}
			});
		}
	}).then(function() {
		this.current_request = this.current_request.nvalue;
		this.current_request['available'] = true;
		this.current_request.nvalue = {};
	}.bind(this)).catch(function() {
		console.log('Error in update creditrequest controller');
	});
};

var new_instance = function(model_dashboard, context) {
	return new CreditRequest(model_dashboard, context);
};

module.exports = new_instance;
var DashboardController = function(model_dashboard, context) {
	this.model_dashboard = model_dashboard;
	if (typeof(context) == 'undefined')
		this.context = this;
	else
		this.context = context;
};

DashboardController.prototype.login_user = function(data, callback) {

	var args = { model_dashboard: this.model_dashboard };

	new Promise((resolve, reject) => {
		args.model_dashboard.login_user(data, resolve);
	}).then((token) => {
		let response_login_user = token != 0;
		callback({
			response_login_user: response_login_user,
			user: data.user,
			token: token != 0 ? token : ''
		});
	});
};

DashboardController.prototype.logout_user = function(data) {
	if ( ! data.user)
		return;

	this.model_dashboard.delete_token(data);
};

DashboardController.prototype.save_creditrequest = function(data) {
	
	if (data.credit_type == 1) {
		data.amount *= 1.05;
	} else if (data.credit_type == 2) {
		data.amount *= 1.07;
	} else if (data.credit_type == 3) {
		data.amount *= 1.12;
	}

	data['time'] = new Date().getTime();

	this.model_dashboard.save_creditrequest(data);
};

DashboardController.prototype.vote_creditrequest = function(data) {
	this.model_dashboard.vote_creditrequest(data);
};

var new_instance = function(model_dashboard, context) {
	return new DashboardController(model_dashboard, context);
}

module.exports = new_instance;
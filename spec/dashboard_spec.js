var dashboard_controller = require("./../controllers/dashboard.js");

describe("Testing", function() {

  it("save_creditrequest", function() {
    // 5% interests
    current_amount = (Math.random() * 10000).toFixed(2);
    let model_1 = {
    	save_creditrequest: function(data) {
    		jasmine.objectContaining(data);

    		let floor_amount = data.amount.toFixed(2);
    		let expected_amount = (current_amount * 1.05).toFixed(2);

    		expect(floor_amount).toBe(expected_amount);
    		expect(data.time).toEqual(jasmine.any(Number));
    	}
    };
  	
  	let dashboard_controller_instance = dashboard_controller(model_1);
  	let test_data = { credit_type: 1, amount: current_amount };
	dashboard_controller_instance.save_creditrequest(test_data);

	// 7% interests
	current_amount = (Math.random() * 10000).toFixed(2);
	model_2 = {
		save_creditrequest: function(data) {
    		jasmine.objectContaining(data);

    		let floor_amount = data.amount.toFixed(2);
    		let expected_amount = (current_amount * 1.07).toFixed(2);
    		expect(floor_amount).toBe(expected_amount);
    		expect(data.time).toEqual(jasmine.any(Number));
    	}	
	}

	dashboard_controller_instance = dashboard_controller(model_2);
	test_data = { credit_type: 2, amount: current_amount };
	dashboard_controller_instance.save_creditrequest(test_data);

	// 12% interests
	current_amount = (Math.random() * 10000).toFixed(2);
	model_3 = {
		save_creditrequest: function(data) {
    		jasmine.objectContaining(data);

    		let floor_amount = data.amount.toFixed(2);
    		let expected_amount = (current_amount * 1.12).toFixed(2);
    		expect(floor_amount).toBe(expected_amount);
    		expect(data.time).toEqual(jasmine.any(Number));
    	}	
	}

	dashboard_controller_instance = dashboard_controller(model_3);
	test_data = { credit_type: 3, amount: current_amount };
	dashboard_controller_instance.save_creditrequest(test_data);
  });
});

// jasmine-node spec
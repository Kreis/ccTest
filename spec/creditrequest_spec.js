let creditrequest = require('./../controllers/creditrequest.js');

describe("Testing", function() {

  it("remove_client", function() {

   	let context = { user_online: {
  		'abc': { 'user': 'abc', 'socket': '11111' },
  		'nombre': { 'user': 'nombre', 'socket': '3123' },
  		'xxyyzz': { 'user': 'xxyyzz', 'socket': '11123111' }
  	} };

  	let creditrequest_instance = creditrequest({}, context);
  	creditrequest_instance.remove_client({ user: 'nombre' });

  	jasmine.objectContaining(context);
  	jasmine.objectContaining(context['user_online']);

	expect(context['user_online']['abc']['user']).toBe('abc');
	expect(context['user_online']['abc']['socket']).toBe('11111');
	expect(context['user_online']['nombre']).toBeUndefined();
	expect(context['user_online']['xxyyzz']['user']).toBe('xxyyzz');
	expect(context['user_online']['xxyyzz']['socket']).toBe('11123111');

  });

});
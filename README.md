# CCTest

Proyecto prototipo de autorización de préstamos crediticios

## Empezando

### Prerequisitos

Requiere installar MySql Server 5.7 o superior
https://dev.mysql.com/downloads/mysql/

### Configuración

En el archivo database/connection.js
encontrará la información de conexión a la base de datos

```
	host: "localhost",
	user: "root",
	password: "secret",
```

### Instalando
Instrucciones por consola
```
> git clone https://gitlab.com/Kreis/ccTest.git

> cd ccTest

> npm install

> node database/migrate.js

> npm start

```
acceder a localhost:8080

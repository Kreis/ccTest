var get_connection = function() {
	var mysql = require('mysql');

	var con = mysql.createConnection({
		host: "localhost",
		user: "root",
		password: "secret",
		multipleStatements: true
	});

	return con;
};

module.exports = get_connection;
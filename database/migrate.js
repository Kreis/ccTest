var con = require('./connection.js')();

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");

    con.query('CREATE DATABASE IF NOT EXISTS cctest_db', function(err, result) {
        if (err) throw err;
        console.log("Database created");
    });

    con.query('USE cctest_db', function(err, result) {
        if (err) throw err;
        console.log('USE cctest_db');
    });

    let query;
    query = `CREATE TABLE IF NOT EXISTS user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    current_token VARCHAR(30) NOT NULL DEFAULT '',
    INDEX name_index (name))`;
    con.query(query, function (err, result) {
        if (err) throw err;
        console.log('created table');
      });

    query = `CREATE TABLE IF NOT EXISTS history(
    id INT PRIMARY KEY AUTO_INCREMENT,
    user VARCHAR(255) NOT NULL,
    start_date VARCHAR(200),
    result INT,
    amount DECIMAL(12,2),
    credit_type INT,
    INDEX user_index (user),
    FOREIGN KEY (user)
    REFERENCES user(name)
    ON DELETE CASCADE)`;
    con.query(query, function (err, result) {
        if (err) throw err;
        console.log('created table');
      });

    query = `CREATE TABLE IF NOT EXISTS votes(
    id INT PRIMARY KEY AUTO_INCREMENT,
    user VARCHAR(255) NOT NULL,
    vote INT,
    id_history INT NOT NULL,
    INDEX user_index (user),
    FOREIGN KEY (user)
    REFERENCES user(name)
    ON DELETE CASCADE,
    FOREIGN KEY (id_history)
    REFERENCES history(id)
    ON DELETE CASCADE)`;
    con.query(query, function (err, result) {
        if (err) throw err;
        console.log('created table');
      });

    query = `
    DROP PROCEDURE IF EXISTS proc_validate_user;
    CREATE PROCEDURE proc_validate_user
    (IN in_name VARCHAR(255), IN in_token VARCHAR(30), OUT out_result INT)
    BEGIN
		START TRANSACTION;

		SELECT FALSE INTO out_result;

		SELECT @current_token := current_token FROM user WHERE name = in_name FOR UPDATE;

		IF @current_token IS NULL
		THEN
			INSERT INTO user (name, current_token) VALUES (in_name, in_token);
			SELECT TRUE INTO out_result;
		ELSE
			IF @current_token = ''
			THEN
				UPDATE user SET current_token = in_token WHERE name = in_name;
				SELECT TRUE INTO out_result;
			END IF;
		END IF;

		COMMIT;
    END ;`;
    con.query(query, function (err, result) {
        if (err) throw err;
        console.log('created procedure');
    });
      
    con.end(function(err) {
	    if (err) throw err;
	    console.log('complete');
    });
});

var con = require('./connection.js')();

con.connect(function(err) {
	if (err) throw err;
	console.log("Connected!");

	con.query('DROP DATABASE IF EXISTS cctest_db', function(err, result) {
		if (err) throw err;
		console.log("Database droped");
	});

	con.end(function(err) {
    	if (err) throw err;
    	console.log('complete');
  	});
});
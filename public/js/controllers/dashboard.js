var MyCtrlDashboard = function(context) {
	if (typeof(context) === "undefined")
		this.context = this;
	else
		this.context = context;
};

MyCtrlDashboard.prototype.update_screen_info = function(user, data) {

	var my_info = {};
	var current_creditrequest = {};

	for (let i in data) {
		let pack = data[ i ];

		if (pack['user'] == user) {

			let creditrequest_active = pack['count_down'] > 0;

			my_info.solicitud_pendiente = creditrequest_active;
			if (creditrequest_active) {
				my_info.positives = pack['positives'];
				my_info.negatives = pack['negatives'];
				my_info.request_time = parse_time(pack['count_down']);
				my_info.amount = pack['amount'];
			}

			if (pack['history']) {
				my_info.my_history = [];
				for (let h in pack['history']) {

					if (pack['history'][ h ]['result'] == null)
						continue;

					let my_history_data = {
						positives: pack['history'][ h ]['positives'],
						negatives: pack['history'][ h ]['negatives']
					};
					if (pack['history'][ h ]['result'])
						my_history_data['approved'] = true;

					my_info.my_history.push(my_history_data);
				}
			}

			continue;
		}

		let arr_history = [];
		for (let i in pack['history']) {
			
			pack['history'][ i ]['approved'] = pack['history'][ i ]['result'] ? true : false;
			arr_history.push(pack['history'][ i ]);
		}

		current_creditrequest[ pack['user'] ] = {
			user: pack['user'],
			positives: pack['positives'],
			negatives: pack['negatives'],
			result: pack['result'],
			history: arr_history
		};
	}

	return { my_info: my_info, current_creditrequest: current_creditrequest };
};

var dashboard_ctrl_instance = new MyCtrlDashboard();
var dashboard_ctrl = function($scope, socket) {
	let my_user_name = get_user();
	$scope.user = my_user_name;
	$scope.other_history = {};
	$scope.current_request = {};

	socket.emit('identify', {user: my_user_name });

	socket.on('update', function(data) {
		console.log('update');

		let screen_info = dashboard_ctrl_instance.update_screen_info(my_user_name, data);

		$scope.solicitud_pendiente = screen_info.my_info.solicitud_pendiente;
		if (screen_info.my_info.solicitud_pendiente) {
			$scope.positives = screen_info.my_info.positives;
			$scope.negatives = screen_info.my_info.negatives;
			$scope.request_time = screen_info.my_info.request_time;
			$scope.amount = screen_info.my_info.amount;
		}
		
		if (screen_info.my_info.my_history && screen_info.my_info.my_history.length > 0) {
			$scope.my_history = screen_info.my_info.my_history;
		}

		let to_retain = {};
		for (let user in screen_info.current_creditrequest) {

			to_retain[user] = true;
			let current = screen_info.current_creditrequest[ user ];

			if ($scope.current_request[ user ]) {

				$scope.current_request[ user ]['user'] = current['user'];
				$scope.current_request[ user ]['positives'] = current['positives'];
				$scope.current_request[ user ]['negatives'] = current['negatives'];

				if ( ! $scope.current_request[ user ]['history'])
					$scope.current_request[ user ]['history'] = current['history'];
				
				continue;
			}

			$scope.current_request[ user ] = {
				user: user,
				positives: 	current['positives'],
				negatives: 	current['negatives'],
				result: 	current['result'],
				history: 	current['history']
			};

		}

		for (let user in $scope.current_request) {
			if ( ! to_retain[ user ])
				delete $scope.current_request[ user ];
		}
	});

	$scope.show_make_new_request = function() {
		$scope.show_capture_request = true;
	};

	$scope.make_new_request = function(credit_type, amount) {
		if (credit_type == 0 || amount <= 0)
			return;

		$scope.show_capture_request = false;
		socket.emit('new_creditrequest', {credit_type: credit_type, amount: amount});
	};

	$scope.vote = function(user_request, the_vote) {
		socket.emit('new_vote', { user_request: user_request, vote: the_vote });
	};

	$scope.show_history_changed = function(user, active_show) {
		$scope.other_history[ user ] = active_show;
	};
};






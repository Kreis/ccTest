
var MyLoginCtrl = function(socket, context) {
	this.socket = socket;

	if (typeof(context) === "undefined")
		this.context = this;
	else
		this.context = context;
};

MyLoginCtrl.prototype.login_user = function(data) {
	if ( ! data.user || data.user == '')
		return;

	this.socket.emit('login_user', data);
};

MyLoginCtrl.prototype.response_login_user = function(data) {
	if ( ! data.response_login_user)
		return false;

	return data.user && data.token && data.user.length > 0 && data.token.length > 0;
};

var login_ctrl_instance = {};
var login_ctrl = function($scope, socket) {
	login_ctrl_instance = new MyLoginCtrl(socket);

	socket.on('response_login_user', function (data) {
		let auth = login_ctrl_instance.response_login_user(data);
		$scope.denied_user = ! auth;
		if (auth)
			 location.replace('http://localhost:8080/dashboard?user='+data.user+'&token='+data.token);
	});
};

var login_user = function() {
	login_ctrl_instance.login_user({ user: $('#id_usuario').val() });
};

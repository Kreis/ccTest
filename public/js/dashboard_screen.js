var get_user = function() {
  var url = new URL(location.href);
  return url.searchParams.get('user');
}

var parse_time = function(time) {

  let minutes = time / (1000 * 60);
  let seconds = (minutes - Math.floor(minutes)) * 60;

  minutes = Math.floor(+minutes);
  seconds = Math.floor(+seconds);

  minutes = minutes < 10 ? '0' + minutes : minutes;
  seconds = seconds < 10 ? '0' + seconds : seconds;

  return minutes + ':' + seconds;
};

var app = angular.module('myApp', []);
app.factory('socket', function ($rootScope) {
  var socket = io.connect();
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});